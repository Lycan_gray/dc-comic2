<?php

    namespace App\Http\Controllers;

    use App\Character;
    use App\Comic;
    use Illuminate\Http\Request;

    class AdminController extends Controller
    {
        public function __construct()
        {
            $this->middleware('auth');
        }

        public function admin()
        {
            $characters = Character::all();
            $comic = Comic::all();
            return view('admin', ['characters' => $characters, 'comics' => $comic]);
        }
    }
