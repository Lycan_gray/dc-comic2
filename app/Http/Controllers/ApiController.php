<?php

    namespace App\Http\Controllers;

    use App\Comic;
    use Illuminate\Http\Request;

    class ApiController extends Controller
    {
        public function index()
        {
            return response()->json(Comic::get(), 200);
        }

        public function show()
        {
            $query = request('q');
            $comic = Comic::where('id', $query)->orWhere('series_title', 'like', '%' . $query . '%')->orWhere('issue', $query)->get();
            if (is_null($comic)) {
                return response()->json(null, 404);
            }
            return response()->json($comic, 200);
        }

        public function showComicList()
        {
            $query = request('q');
            $comic = Comic::where('series_title', 'like', '%' . $query . '%')->orWhere('issue', $query)->orderBy('issue', 'ASC')->get();
            if (is_null($comic)) {
                return response()->json(null, 404);
            }
            return response()->json($comic, 200);
        }
//
//    public function store(Request $request)
//    {
//        $rules = [
//            'hero' => 'required|max:16',
//        ];
////            $validator = Validator::make($request->all(). $rules);
//        $validator = Validator::make($request->all(), $rules);
//        if ($validator->fails()) {
//            return response()->json($validator->errors(), 400);
//        }
//        $comic = Comic::create($request->all());
//        return response()->json($comic, 201);
//    }
//
//    public function update(Request $request, Comic $comic)
//    {
//        $comic->update($request->all());
//        return response()->json($comic, 200);
//    }
//
//    public function delete(Request $request, Comic $comic)
//    {
//        $comic->delete();
//        return response()->json(null, 204);
//    }
    }
