<?php

    namespace App\Http\Controllers;

    use App\Comic;
    use App\Hero;
    use App\User;
    use DateTime;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Route;
    use Imgur;


    class HomeController extends Controller
    {
        /**
         * Create a new controller instance.
         *
         * @return void
         */
//        public function __construct()
//        {
//            $this->middleware('auth');
//        }

        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('home');
        }

        public function showGlobalInventory()
        {
//            $user = Auth::user();
//            $comicsAvailable = Comic::all();
//            return view('inventory', ['currentUser' => $user, 'comicList' => $comicsAvailable]);

            $user = Auth::user();
            $searchInput = request('searchTerm');
            $searchInputStripped = str_replace("#", "", $searchInput);
            if (request()->has('searchTerm')) {
                if (request()->has('order') && request('order') == 'ASC') {
                    $order = 'ASC';
                    $comicsAvailable = Comic::where('series_title', 'LIKE', '%' . $searchInputStripped . '%')->orWhere('issue', $searchInputStripped)->orderBy('issue', $order)->get();
                } else if (request()->has('order') && request('order') == 'DESC') {
                    $order = "DESC";
                    $comicsAvailable = Comic::where('series_title', 'LIKE', '%' . $searchInputStripped . '%')->orWhere('issue', $searchInputStripped)->orderBy('issue', $order)->get();
                } else {
                    $comicsAvailable = Comic::where('series_title', 'LIKE', '%' . $searchInputStripped . '%')->orWhere('issue', $searchInputStripped)->get();
                }
            } else {
                $comicsAvailable = Comic::all();
            }
            return view('inventory', ['currentUser' => $user, 'comicList' => $comicsAvailable, 'heroid' => $searchInput]);
        }

        public function showUserInventory()
        {
            $user = Auth::user();
            $userId = $user->id;
            $searchInput = request('searchTerm');
            $order = request('order');
            if ($order == "ASC") {
                $inventory = User::find($userId)->inventory->sortBy('issue');
                if ($searchInput == "") {
                    $result = $inventory;
                } else {
                    $result = $inventory->filter(function ($value, $key) {
                        $searchInput = request('searchTerm');
                        $searchInputStripped = str_replace("#", "", $searchInput);
                        if (stripos($value->series_title, $searchInputStripped) !== false) {
                            return $value;
                        } else if (stripos($value->issue, $searchInputStripped) !== false) {
                            return $value;
                        }
                    });
                }

            } else if ($order == "DESC") {
                $inventory = User::find($userId)->inventory->sortByDesc('issue');
                if ($searchInput == "") {
                    $result = $inventory;
                } else {
                    $result = $inventory->filter(function ($value, $key) {
                        $searchInput = request('searchTerm');
                        $searchInputStripped = str_replace("#", "", $searchInput);
                        if (stripos($value->series_title, $searchInputStripped) !== false) {
                            return $value;
                        } else if (stripos($value->issue, $searchInputStripped) !== false) {
                            return $value;
                        }
                    });
                }

            } else {
                $inventory = User::find($userId)->inventory;
                if ($searchInput == "") {
                    $result = $inventory;
                } else {
                    $result = $inventory->filter(function ($value, $key) {
                        $searchInput = request('searchTerm');
                        $searchInputStripped = str_replace("#", "", $searchInput);
                        if (stripos($value->series_title, $searchInputStripped) !== false) {
                            return $value;
                        } else if (stripos($value->issue, $searchInputStripped) !== false) {
                            return $value;
                        }
                    });
                }
            }

            return view('user.inventory', ['currentUser' => $user, 'userInventory' => $result, 'heroid' => $searchInput, 'order' => $order]);
        }

        public function addToInventory(Request $request)
        {
            $comicId = request('addComicID');
            $userId = Auth::user()->id;
            DB::table('comic_user')->insert(["user_id" => $userId, 'comic_id' => $comicId]);
            return back();
        }

        public function store(Request $request)
        {
            $currentPath = Route::getFacadeRoot()->current()->uri();
            $dt = new DateTime();
            $dt = $dt->format('Y-m-d H:i:s');
            if ($currentPath === "createHero") {
                $addHero = request('hero');
                DB::table('character')->insert(
                    ['id' => '0',
                        'characterName' => $addHero, 'created_at' => $dt, 'updated_at' => $dt]
                );
                return back();
            } else if ($currentPath === "createComic") {
                $heroSelect = request('heroSelect');
                $imageFile = $request->file('coverUrl');
                $image = $imageFile->get();
                $imgurImg = Imgur::setHeaders([
                    'headers' => [
                        'authorization' => 'Client-ID ' . env('IMGUR_CLIENT_ID', 'f2a5f8db03fe988'),
                        'content-type' => 'application/x-www-form-urlencoded',
                    ]
                ])->setFormParams([
                    'form_params' => [
                        'image' => $image,
                    ]
                ])->upload($image);

                $imgurLink = $imgurImg->link();
                $comicTitle = request('comicTitle');
                $issueNum = request('issueNum');
                $releaseDate = request('releaseDate');
                $writer = request('writer');
                $illustrator = request('illustrator');
                $coverIllustrator = request('coverIllustrator');
                DB::table('comic')->insert(
                    ['id' => '0',
                        'character_id' => $heroSelect, 'cover_url' => $imgurLink, 'cover_illustrator' => $coverIllustrator, 'series_title' => $comicTitle, 'issue' => $issueNum, 'comic_writer' => $writer, 'comic_illustrator' => $illustrator, 'release_date' => $releaseDate, 'created_at' => $dt, 'updated_at' => $dt]
                );
                return back();
            } else {
                dd('Incorrect');
            }
        }

    }
