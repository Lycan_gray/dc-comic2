<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateComicsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('comic', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('character_id')->unsigned();
                $table->string('cover_url');
                $table->string('cover_illustrator');
                $table->string('series_title');
                $table->integer('issue');
                $table->string('comic_writer');
                $table->string('comic_illustrator');
                $table->date('release_date');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('comic');
        }
    }
