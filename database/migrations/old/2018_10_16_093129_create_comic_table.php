<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateComicTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('comic', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('hero_id')->unsigned();
                $table->foreign('hero_id')->references('id')->on('hero');
                $table->string('coverLink');
                $table->string('hero');
                $table->string('issue');
                $table->string('release');
                $table->string('writer');
                $table->string('illustrator');
                $table->string('coverIllustrator');
                $table->timestamps();
            });

        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('comic');
        }
    }
