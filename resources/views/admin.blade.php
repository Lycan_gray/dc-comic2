@extends('layout')
@section('content')
</div>
    @include('partials/addcomic')
    <div class="container mt-5">
        <h1>ADMIN PANEL</h1>
        <button class="btn btn-primary" data-toggle="modal" data-target="#addComic">Add Comic to Database</button>
        <button class="btn btn-primary" data-toggle="modal" data-target="#addHero">Add Hero to Database</button>
        <ul class="mt-3">
            @foreach($characters as $character)
                <li>{{$character->characterName}}</li>
            @endforeach
        </ul>
        <ul>
            @foreach($comics as $comic)
                <li>{{$comic->series_title}} #{{$comic->issue}}</li>
            @endforeach
        </ul>
    </div>

@endsection
