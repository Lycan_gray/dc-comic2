@extends('layout')
@section('content')
    <div class="sortbyBar p-3">
        <div class="container-fluid">
            <form method="post">
                {{ csrf_field() }}
                <div class="row justify-content-end">
                    <div class="col-12 col-md-1 mb-2">
                        <input class="form-control mr-sm-2" value="{{$heroid}}" type="text" name="searchTerm" id="id"
                               placeholder="Search"
                               aria-label="Search">
                    </div>
                    <div class="col-6 col-md-1">
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-primary active">
                                <input type="radio" name="order" value="ASC" id="option1" autocomplete="off"><i
                                    class="fas fa-sort-numeric-up"></i>
                            </label>
                            <label class="btn btn-primary">
                                <input type="radio" name="order" value="DESC" id="option2" autocomplete="off"><i
                                    class="fas fa-sort-numeric-down"></i>
                            </label>
                        </div>
                    </div>
                    <div class="col-6 col-md-1">
                        <button class="btn btn-primary" type="submit">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
    <div class="container-fluid mt-5">
        <div class="row justify-content-between">
            <div class="col-7">
                @if(count($comicList) == 0)
                    <h2>No results found for <strong>"{{$heroid}}"</strong></h2>
                @else
                    @if($heroid == "")
                        <h2>Available Comics (<span class="text-muted">{{count($comicList)}}</span>)</h2>
                    @else
                        <h2>Results for: <span class="font-weight-bold">"{{$heroid}}"</span> <span
                                class="text-muted">({{count($comicList)}})</span></h2>
                    @endif
                @endif
            </div>
        </div>
        <hr>
        <div class="row">
            @foreach ($comicList as $comic)
                <div class="col-12 col-sm-6 col-md-3 col-lg-1 mb-2">
                    <img src="{{$comic -> cover_url}}" class="comic img-fluid"
                         alt="{{$comic -> series_title . " #" . $comic -> issue}}">
                    <small class="font-weight-bold">{{$comic -> series_title }} #{{$comic -> issue}}</small>
                </div>
            @endforeach
        </div>
    </div>
@endsection
