{{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Large modal</button>--}}

<div id="addComic" class="modal" tabindex="-1" role="dialog" aria-labelledby="Add Comic Modal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Add Comic</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="/createComic" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!--- Select Hero -->
                    <div class="row">
                        <div class="col-2">
                            <div class="form-group">
                                <label for="heroSelect">Select Hero</label>
                                <select name="heroSelect" id="heroSelect" class="form-control">
                                    @foreach($characters as $character)
                                        <option value="{{$character->id}}">{{$character->characterName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!--- Hero Title / Story Title -->
                        <div class="col-6">
                            <div class="form-group">
                                <label for="comicTitle">Title of Comic</label>
                                <input id="comicTitle" name="comicTitle" type="text"
                                       placeholder="Hero Title / Story Title"
                                       class="form-control">
                            </div>
                        </div>
                        <!--- Cover Link -->
                        <div class="col-2">
                            <div class="form-group">
                                <label for="coverUrl">Cover Image</label>
                                <input type="file" placeholder="Cover Image URL" name="coverUrl" id="coverUrl"
                                       class="form-control-file">
                            </div>
                        </div>

                        <!--- Issue -->
                        <div class="col-2">
                            <div class="form-group">
                                <label for="issueNum">Issue Number</label>
                                <input id="issueNum" name="issueNum" type="number" placeholder="#" class="form-control">
                            </div>
                        </div>
                        <!-- Release Date -->
                        <div class="col-3">
                            <div class="form-group">
                                <label for="releaseDate">Release Date</label>
                                <input id="releaseDate" name="releaseDate" type="date" class="form-control">
                            </div>
                        </div>
                        <!--- Writer -->
                        <div class="col-3">
                            <div class="form-group">
                                <label for="writer">Writer(s)</label>
                                <input id="writer" name="writer" type="text" placeholder="John Doe"
                                       class="form-control">
                            </div>
                        </div>
                        <!--- Illustrator -->
                        <div class="col-3">
                            <div class="form-group">
                                <label for="illustrator">Illustrator(s)</label>
                                <input id="illustrator" name="illustrator" type="text" placeholder="Jake Doe, Jane Doe"
                                       class="form-control">
                            </div>
                        </div>
                        <!--- Cover Illustrator -->
                        <div class="col-3">
                            <div class="form-group">
                                <label for="coverIllustrator">Cover Illustrator(s)</label>
                                <input id="coverIllustrator" name="coverIllustrator" type="text" placeholder="Bert Doe"
                                       class="form-control">
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary float-right" value="Add Comic">
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="addHero" class="modal" tabindex="-1" role="dialog" aria-labelledby="add Hero Modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Hero</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="/createHero" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="hero">Hero Name</label>
                        <input type="text" name="hero" id="hero" class="form-control"></div>
            </div>
            <div class="modal-footer">
                <input class="btn btn-primary" type="submit">
                </form>
            </div>
        </div>
    </div>
</div>
