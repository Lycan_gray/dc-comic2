@extends('layout')
@section('content')
    <div class="sortbyBar p-3 sticky-top">
        <div class="container-fluid">
            <form method="post">
                {{ csrf_field() }}
                <div class="row justify-content-end">
                    <div class="col-12 col-md-1 mb-2">
                        <input class="form-control mr-sm-2" value="{{$heroid}}" type="text" name="searchTerm" id="id"
                               placeholder="Search"
                               aria-label="Search">
                    </div>
                    <div class="col-6 col-md-2">
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-primary @if($order == "ASC") active @else @endif">
                                <input type="radio" name="order" value="ASC" id="option1" autocomplete="off"><i
                                    class="fas fa-sort-numeric-up" @if($order == "ASC") checked @else @endif></i>
                            </label>
                            <label class="btn btn-primary @if($order == "DESC") active @else @endif">
                                <input type="radio" name="order" value="DESC" id="option2" autocomplete="off"><i
                                    class="fas fa-sort-numeric-down" @if($order == "ASC") checked @else @endif></i>
                            </label>
                        </div>
                        <button class="btn btn-primary" type="submit">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
    <div class="container-fluid mt-5">
        <div class="row justify-content-between">
            <div class="col-12 col-md-7 mt-3 mt-md-0">
                @if(count($userInventory) == 0)
                    <h2>No results found for <strong>"{{$heroid}}"</strong></h2>
                @else
                    @if($heroid == "")
                        <h2>Your Comics (<span class="text-muted">{{count($userInventory)}}</span>)</h2>
                    @else
                        <h2>Results for: <span class="font-weight-bold">"{{$heroid}}"</span> <span
                                class="text-muted">({{count($userInventory)}})</span></h2>
                    @endif
                @endif
            </div>
            <div class="col-12 col-md-4 order-first order-md-last">
                <select name="comicAdd" class="selectHero w-100"></select>
            </div>
            <div class="col-12 col-md-1 order-first order-md-last">
                <form method="post" action="/addToInventory" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input id="addComicName" type="hidden">
                    <input id="addComicID" name="addComicID" type="hidden">
                    <input type="submit" class="btn btn-primary w-100 mt-2 mt-md-0" value="Add Comic">
                </form>
            </div>
        </div>
        <hr>
        <div class="row text-center">
            @foreach($userInventory as $inventoryItem)
                <div class="col-12 col-sm-6 col-md-3 col-lg-1 mb-2">
                    <img src="{{$inventoryItem -> cover_url}}" class="comic img-fluid"
                         alt="{{$inventoryItem -> series_title . " #" . $inventoryItem -> issue}}">
                    <br>
                    <p class="font-weight-bold mt-1">{{$inventoryItem -> series_title }}
                        #{{$inventoryItem -> issue}}</p>
                </div>
            @endforeach
        </div>
    </div>
@endsection
