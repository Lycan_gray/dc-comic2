<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

    Route::get('comics', 'ApiController@index');
    Route::get('comics', 'ApiController@show');
    Route::get('comicsList', 'ApiController@showComicList');
    //    Route::post('comics', 'apiController@store');
    //    Route::put('comics/{comic}', 'capiController@update');
    //    Route::delete('comics/{comic}', 'capiController@delete');
